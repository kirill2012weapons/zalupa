import Vuex from 'vuex';
import axios from 'axios';
import Vue from 'vue'

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        axios: axios,
        auth: {
            token: '',
            isLogin: false,
        },
    },
    mutations: {
        axiosInit(state) {
            const tokenUser = localStorage.getItem('user-token');
            if (tokenUser) {
                state
                    .axios
                    .defaults
                    .headers
                    .common['Authorization'] = 'Bearer ' + tokenUser;
                state.auth.token    = tokenUser;
                state.auth.isLogin  = true;
            }
        },
        axiosAddToken(state, tokenUser) {
            if (tokenUser) {
                localStorage.setItem('user-token', tokenUser);
                state
                    .axios
                    .defaults
                    .headers
                    .common['Authorization'] = 'Bearer ' + tokenUser;
                state.auth.token = tokenUser;
                state.auth.isLogin = true;
            }
        },
        axiosLogout(state, tokenUser) {
            if (tokenUser) {
                localStorage.removeItem('user-token');
                state
                    .axios
                    .defaults
                    .headers
                    .common['Authorization'] = '';
                state.auth.token = '';
                state.auth.isLogin = false;
            }
        },
    },
});