import jQuery from "jquery";
import 'popper.js';
import "bootstrap";

window.$ = jQuery;

import Vue from 'vue/dist/vue.js';
/**
 * Get Store (Vuex)
 */
import store from './../vue/vuex/vuex';
import {mapMutations, mapState} from 'vuex';
import login from './../vue/login/login';
import loader from './../vue/helpers/loader/loader';

window.root = new Vue({
    el: '#root',
    store,
    data: function() {
        return {
            title: 'Test',
        }
    },
    beforeMount () {
        this.$options.store.commit('axiosInit');
    },
    components: {
        'login':    login,
        'loader':   loader,
    },
});