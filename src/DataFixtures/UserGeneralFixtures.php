<?php


namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

class UserGeneralFixtures extends Fixture implements FixtureGroupInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var FakerGenerator
     */
    private $faker;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder  = $encoder;
        $this->faker    = FakerFactory::create();
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 30; $i++) {
            $user = new User();
            $roles = $i >= 20 ? ['ROLE_USER', 'ROLE_MANAGER'] : ['ROLE_USER'];
            $user->setRoles($roles);
            $user->setPassword(
                $this->encoder->encodePassword($user, $this->faker->password(6, 20))
            );
            $user->setEmail( $this->faker->email );
            $user->setUsername( $this->faker->userName );
            $user->setEnabled(true);
            $manager->persist($user);
        }

        $adminUser = new User();
        $adminUser->setRoles(['ROLE_SUPER_ADMIN']);
        $adminUser->setPassword(
            $this->encoder->encodePassword($adminUser, 'admin')
        );
        $adminUser->setEmail( 'kirill2012weapons@gmail.com' );
        $adminUser->setUsername( 'admin' );
        $adminUser->setEnabled(true);
        $manager->persist($adminUser);

        $manager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups() : array
    {
        return ['users-create'];
    }
}