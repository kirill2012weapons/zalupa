<?php


namespace App\Controller\FOS;


use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class UserController extends AbstractFOSRestController
{
    /**
     * GET Current User
     *
     * @param $security Security
     * @return Response
     * @Get("/api/user", name="fos_get_current_user")
     */
    public function getCurrentUser(Security $security)
    {
        $data = $security->getUser();
        $view = $this->view($data, Response::HTTP_OK);

        return $this->handleView($view);
    }
}