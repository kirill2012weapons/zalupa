<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Sonata\UserBundle\Entity\BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Many Users have Many Teams.
     *
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Team", inversedBy="users")
     * @ORM\JoinTable(name="users_teams")
     */
    private $teams;

    /**
     * One User can managed a lot of teams
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Team", mappedBy="manager")
     */
    private $managedTeams;

    public function __construct()
    {
        parent::__construct();
        $this->teams        = new ArrayCollection();
        $this->managedTeams = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return ArrayCollection
     */
    public function getManagedTeams()
    {
        return $this->managedTeams;
    }

    /**
     * @param ArrayCollection $managedTeams
     */
    public function setManagedTeams(ArrayCollection $managedTeams)
    {
        $this->managedTeams = $managedTeams;
    }

    /**
     * @return ArrayCollection
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * @param ArrayCollection $teams
     */
    public function setTeams(ArrayCollection $teams)
    {
        $this->teams = $teams;
    }

    /**
     * @param Team $team
     * @return $this
     */
    public function addTeam(Team $team)
    {
        if (!$this->getTeams()->contains($team)) {
            $this->getTeams()->add($team);
        }

        return $this;
    }

    /**
     * @param Team $team
     * @return $this
     */
    public function removeTeam(Team $team)
    {
        if ($this->getTeams()->contains($team)) {
            $this->getTeams()->removeElement($team);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getUsername() . ' - ' . $this->getFullname();
    }
}