<?php


namespace App\Service\Admin;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class TeamAdmin extends AbstractAdmin
{
    private $em;

    public function __construct($code, $class, $baseControllerName, EntityManagerInterface $entityManager)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->em = $entityManager;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('hello');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('title', TextType::class, ['label' => 'Title'])
            ->add('description', TextType::class, ['label' => 'Description'])
            ->add('manager', EntityType::class, ['label' => 'Manager'])
            ->add('users', CollectionType::class, ['label' => 'Members'])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ]
            ])
        ;
    }
    
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('title', TextType::class)

            ->add('description', TextareaType::class, [
                'attr' => [
                    'rows' => 5,
                ]
            ])

            ->add('manager', ChoiceType::class, [
                'choices' => $this->em
                    ->getRepository(User::class)
                    ->findByRole('ROLE_MANAGER'),
                'choice_label' => function ($choice, $key, $value) {
                    return $choice instanceof User ?
                        $choice->getUsername() . (!empty(trim($choice->getFullname())) ? ' (' . $choice->getFullname() . ')' : '')
                        : '';
                },
                'choice_value' => function (?User $entity) {
                    return $entity ? $entity->getId() : '';
                },
            ])

            ->add('users', CollectionType::class, [
                'by_reference' => false,
                'entry_type'    => EntityType::class,
                'entry_options' => [
                    'class' => User::class,
                    'choice_value' => function (?User $entity) {
                        return $entity ? $entity->getId() : '';
                    },
                    'choice_label' => function (?User $entity) {
                        return $entity ?
                            $entity->getUsername() . (!empty(trim($entity->getFullname())) ? ' (' . $entity->getFullname() . ')' : '')
                            : '';
                    },
                ],
                'allow_add'     => true,
                'allow_delete'  => true,
                'delete_empty' => function (User $user = null) {
                    return null === $user || empty($user->getUsername());
                },
            ])
        ;
    }
}